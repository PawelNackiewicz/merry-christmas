```c
#include <stdio.h>

int main()
{
    char string[] = {87, 101, 115, 111, 108, 121, 99, 104, 32, 83, 119, 105, 97, 116, 33};
    printf("%s\n", string);
 
    char string_2[] = {0x57, 0x65, 0x73, 0x6F, 0x6C, 0x79, 0x63, 0x68, 0x20, 0x53, 0x77, 0x69, 0x61, 0x74, 0x21};
    printf("%s\n", string_2);
 
    printf("Wesolych Swiat");
 
    char string_3[] = {68, 122, 105, 119, 110, 101, 46, 46, 46, 32, 117, 32, 109, 110, 105, 101, 32, 100, 122, 105, 97, 108, 97};
    printf("%s\n", string_3);
 
    printf("Dziwne... u mnie dziala");

    return 0;
}
```

```py
    import array as arr
    password = arr.array('i', [68, 122, 105, 119, 110, 101, 46, 46, 46, 32, 117, 32, 109, 110, 105, 101, 32, 100, 122, 105, 97, 108, 97]);
    for char in password:
        print(chr(char))
```